FROM node:18-alpine

WORKDIR /app

COPY package.json .

RUN npm install

COPY . .

RUN sed -ie 's%"preview": "vite preview"$%"preview": "vite preview --host"%g' package.json

RUN npm run build

EXPOSE 4173

CMD [ "npm", "run", "preview" ]